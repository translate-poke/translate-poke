'user strict';
/**
 * Test ../src/net-util.js file
 */
const expect = require('chai').expect;
const express = require('express');
const nock = require('nock');
const respObj = require('./response');
const {
  getPokemonId,
  getPokemonDescr,
  getShakespearTranslation
} = require('../src/net-util');

const app = express();
const port = 12345;

describe('test getPokemonId', () => {

  afterEach(() => {
    nock.cleanAll();
  });

  it('should return the identifier of the specified pokemon name', async () => {
    
    nock('https://pokeapi.co')
      .get('/api/v2/pokemon/charizard')
      .reply(200, respObj);

    const resp = await getPokemonId('charizard')
    expect(typeof resp).to.equal('number');
    expect(resp).to.equal(6);
  });
  
  it('should return a specific error object when the identifier of the specified pokemon name does not exist', async () => {

    nock('https://pokeapi.co')
      .get('/api/v2/pokemon/not_existent')
      .replyWithError(respObj);

    try {
      const resp = await getPokemonId('not_existent')
    } catch (error) {
      expect(error.code).to.equal(404);
      expect(error.error).to.equal('not found');
      expect(error.message).to.equal('Request failed with status code 404');
      expect(error.customMsg).to.equal('getting pokemon id for name "not_existent"');
    }
  });
});

describe('test getPokemonDescr', () => {

  afterEach(() => {
    nock.cleanAll();
  });

  it('should return the description of the specified pokemon identifier', async () => {

    nock('https://pokeapi.co')
      .get('/api/v2/characteristic/6')
      .reply(200, {
        descriptions: [
          {
            description: 'Aime courir',
            language: {
              "name": "fr",
              "url": "https://pokeapi.co/api/v2/language/9/"
            }
          },
          {
            description: 'Le gusta correr',
            language: {
              "name": "es",
              "url": "https://pokeapi.co/api/v2/language/7/"
            }
          },
          {
            description: 'Likes to run',
            language: {
              "name": "en",
              "url": "https://pokeapi.co/api/v2/language/9/"
            }
          }
        ]
      });

    const resp = await getPokemonDescr('6');
    expect(typeof resp).to.equal('string');
    expect(resp).to.equal('Likes to run');
  });
  
  it('should return a specific error object when the description of the specified pokemon name does not exist', async () => {

    nock('https://pokeapi.co')
      .get('/api/v2/characteristic/690234987')
      .replyWithError(respObj);

    try {
      const resp = await getPokemonDescr('690234987');
    } catch (error) {
      expect(error.code).to.equal(404);
      expect(error.error).to.equal('not found');
      expect(error.message).to.equal('Request failed with status code 404');
      expect(error.customMsg).to.equal('getting pokemon description for id "690234987"');
    }
  });
});

describe('test getShakespearTranslation', () => {

  afterEach(() => {
    nock.cleanAll();
  });

  it('should return the Shakespeare translation of the pokemon description', async () => {

    nock('https://api.funtranslations.com')
      .get('/translate/shakespeare.json?text=' + escape('Likes to run'))
      .reply(200, {
        contents: {
          translated: 'Likes to runneth'
        }
      });

    const resp = await getShakespearTranslation('Likes to run');
    console.log(resp);
    expect(typeof resp).to.equal('string');
    expect(resp).to.equal('Likes to runneth');
  });
  
  it('should return a specific error object when the Shakespeare translation of the specified pokemon description does not exist', async () => {

    nock('https://api.funtranslations.com')
      .get('/translate/shakespeare.json?text=' + escape('Likes to run'))
      .replyWithError(respObj);

    try {
      const resp = await getShakespearTranslation('Likes to run');
    } catch (error) {
      expect(error.code).to.equal(404);
      expect(error.error).to.equal('not found');
      expect(error.message).to.equal('Request failed with status code 404');
      expect(error.customMsg).to.equal('getting Shakespeare translation for msg "Likes to run"');
    }
  });
});