module.exports = {
  id: 6,
  error: 'not found',
  message: 'Request failed with status code 404',
  customMsg: 'getting pokemon id for name "not_existent"',
  code: 404
};