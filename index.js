/**
 * @author: Alessandro Polidori
 */
'use strict';
const {
  getPokemonId,
  getPokemonDescr,
  getShakespearTranslation
} = require('./src/net-util');

(async () => {
  'use strict';
  const express = require('express');
  const app = express();
  const PORT = process.argv[2] || 80;

  // REST API interface: it accepts the parameter name
  app.get('/pokemon/:name', async (req, res) => {
    const pokemonName = req.params.name;
    try {
      const pokemonId = await getPokemonId(pokemonName);
      const pokemonDescr = await getPokemonDescr(pokemonId);
      const translation = await getShakespearTranslation(pokemonDescr);
      console.log(`request for pokemon "${pokemonName}" - id: "${pokemonId}" descr "${pokemonDescr}" translation: ${translation}`);
      res.send({
        name: pokemonName,
        description: translation
      });
    } catch (error) {
      console.error(`${error.customMsg}: ${error.message}`);
      const obj = {
        error: 'not found',
        message: error.message,
        customMsg: error.customMsg
      };
      if (error.response && error.response.status) {
        obj.code = error.response.status;
      }
      res.send(obj);
    }
  });

  app.listen(PORT, () => {
    console.log(`listening at http://localhost:${PORT}`)
  });
})();