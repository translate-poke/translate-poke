# Shakespeare Pokemon application

## Description

Shakespeare Pokemon application by [Alessandro Polidori.](http://alessandropolidori.com/)

This application exposes a single endpoint GET `/pokemon/:pokemonName`. It returns the shakespeare's translation of the Pokemon description. It uses the following web services:

- [PokéAPI](https://pokeapi.com): to get the Pokemon's description
- [Shakespeare translator](https://funtranslation.com/api/shakespeare): to get the Shakespeare's translation of a text

## Files

The following are the content of the repository:

- `index.js:` the entrypoint of the application
- `src/net-util.js:` contains the network utilities used to make HTTP requests
- `Dockerfile:` used to create the docker image to be used to run the container with the application

## Requirements

- [Node.js](https://nodejs.org/en/download/) v12 (12.21.0) or later (how to intall [here](https://nodejs.org/en/download/package-manager/))
- [Docker](https://docs.docker.com/get-docker/)

## How to use for development

### Install dependencies

From the root of the project, install dependencies with NPM:

```bash
npm i
```

### Run the server

Execute:

```bash
npm start
```

or

```bash
node index.js
```

The server listen on localhost on HTTPS 80 default port (check if you have the right permission to listen to 80). If you want to use another port:

```bash
npm start <PORT>
```

or

```bash
node index.js <PORT>
```

example:

```bash
npm start 9292
```

### Usage of the provided REST API

You can use [`curl`](https://curl.se/) or [`http`](https://httpie.io/):

```bash
curl http://<ADDRESS>:<PORT>/pokemon/:pokemon_name
```

example:

```bash
curl http://localhost/pokemon/charizard
curl http://localhost:9292/pokemon/charizard
```

or

```bash
http http://localhost/pokemon/charizard
http http://localhost:9292/pokemon/charizard
```


## How to use for production

1. From the root of the project, create the docker image:

```bash
docker build -t alepolidori/translate-poke .
```

I've already created an image and pushed it on my [Docker Hub repo](https://hub.docker.com/repository/docker/alepolidori/translate-poke) using:

```bash
docker build -t alepolidori/translate-poke .
docker push alepolidori/translate-poke
```

so, as an alternative to directly create the Docker image, you can download the already created one with:

```bash
docker pull alepolidori/translate-poke
```

2. Run the container:

```bash
docker run -p 80:80 --name pokeapp -d alepolidori/translate-poke
```

The docker container exposes the port 80. If the port is already in use, try to use another one, for example:

```bash
docker run -p 9292:80 --name pokeapp -d alepolidori/translate-poke
```

3. Now you can use the provided REST API

Using `curl`:

```bash
curl http://<YOUR_IP>/pokemon/charizard
```

or using `httpie`:

```bash
http http://<YOUR_IP>/pokemon/charizard
```

or you can open your web browser to the URL [`http://localhost/pokemon/charizard`](http://localhost/pokemon/charizard).

Please note that if you have specified a custom port to run the container, you have to use it in the URL. For example using the port 9292:

```bash
curl http://localhost:9292/pokemon/charizard
```

If you want to stop the container:

```bash
docker stop pokeapp
```

## Notes

- This is a git repo, so you can take a look at the git log history with `git log.`
- I've used the REST API [`https://pokeapi.co/api/v2/pokemon/{id or name}/`](https://pokeapi.co/docs/v2#pokemon) to get the pokemon `id` using the pokemon's name
- I've used the REST API [`https://pokeapi.co/api/v2/characteristic/{id}/`](https://pokeapi.co/docs/v2#characteristics) to get the pokemon's description using the obtained `id`
- I've used the REST API [`https://api.funtranslations.com/translate/shakespeare.json`](https://funtranslations.com/api/shakespeare) to get the Shakespeare's translation of the pokemon's description
- The test suite can be extended to test other form of errors, for example the error code 429 relative to requests limit or the 500 error to test server error
- Once you have the container you can put it in production

## License

The MIT License (MIT)

Copyright (c) 2020 Alessandro Polidori <alessandro.polidori@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
