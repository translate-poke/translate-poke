/**
 * @author: Alessandro Polidori
 */
'use strict';
const axios = require('axios');
const TRANSLATION_URL = 'https://api.funtranslations.com/translate/shakespeare.json?text=';
const POKEAPI_URL = 'https://pokeapi.co/api/v2/';

/**
 * Return the Shakespeare's translation of the provided message.
 * 
 * @method getShakespearTranslation
 * @param {string} msg A message to translate
 * @return {string} The Shakespeare's translation of the provided message.
 */
async function getShakespearTranslation(msg) {
  try {
    const url = TRANSLATION_URL + msg;
    const resp = await axios.get(url);
    if (resp.status === 200 && resp.data && resp.data.contents && resp.data.contents.translated) {
      return resp.data.contents.translated;
    }
    throw new Error('data not found');
  } catch(error) {
    error.customMsg = `getting Shakespeare translation for msg "${msg}"`;
    throw error;
  }
}

/**
 * Return the identifier number of a Pokemon.
 * 
 * @method getPokemonId
 * @param {string} name The name of a Pokemon
 * @return {string} The identifier number of the specified Pokemon.
 */
async function getPokemonId(name) {
  try {
    const url = POKEAPI_URL + 'pokemon/' + name;
    const resp = await axios.get(url);
    if (resp.status === 200 && resp.data && resp.data.id) {
      return resp.data.id;
    }
    throw new Error('data not found');
  } catch (error) {
    error.customMsg = `getting pokemon id for name "${name}"`;
    throw error;
  }
}

/**
 * Return the description of the Pokemon.
 * 
 * @method getPokemonDescr
 * @param {string} id The Pokemon identifier number
 * @return {string} The Pokemon description.
 */
 async function getPokemonDescr(id) {
  try {
    const url = POKEAPI_URL + 'characteristic/' + id;
    const resp = await axios.get(url);
    if (resp.status === 200 && resp.data && resp.data.descriptions) {
      let tempDescr;
      for (let i = 0; i < resp.data.descriptions.length; i++) {
        tempDescr = resp.data.descriptions[i];
        if (tempDescr.language.name === 'en') {
          return tempDescr.description;
        }
      }
    }
    throw new Error('data not found');
  } catch(error) {
    error.customMsg = `getting pokemon description for id "${id}"`;
    throw error;
  }
}

exports.getPokemonId = getPokemonId;
exports.getPokemonDescr = getPokemonDescr;
exports.getShakespearTranslation = getShakespearTranslation;